# Gcc Avr


## GNU C Compiler for avr

This package has been compiled to target the  avr architecture.
GCC is a C compiler.

This package is primarily for avr developers and cross-compilers and
is not needed by normal users or developers.
