gcc-avr (1:14.2.0-2) unstable; urgency=medium

  [ Keith Packard ]
  * Revert patch leading to code size increase. Closes: #1086319.

 -- Steve Meliza <swm@swm1.com>  Wed, 30 Oct 2024 20:03:32 -0700

gcc-avr (1:14.2.0-1) unstable; urgency=low
  * Switch to mainstream gcc (closes: #932989, #917390, #808396, #719728)
  * d/rules: Enable Rust support.

 -- Steve Meliza <swm@swm1.com>  Fri, 25 Oct 2024 19:35:40 -0700

gcc-avr (1:7.3.0+Atmel3.7.0-2) unstable; urgency=high

  * Update to autoconf2.72 (closes: #1080109)
  * debian/control: Increase Standards-Version to 4.7.0

 -- Steve Meliza <swm@swm1.com>  Fri, 30 Aug 2024 17:38:21 -0700

gcc-avr (1:7.3.0+Atmel3.7.0-1) unstable; urgency=low

  * New Maintainer (closes: #1051419)  
  * New upstream release
  * debian/control: Increase Standards-Version to 4.6.2
  * debian/control: Increase compat level to 13
  * debian/control: autoconf2.69 changed to autoconf
  * debian/copyright: Update to DEP5 format

 -- Steve Meliza <swm@swm1.com>  Fri, 27 Oct 2023 22:26:42 -0700

gcc-avr (1:5.4.0+Atmel3.6.2-6) unstable; urgency=medium

  * Remove unnecessary constraints
  * Update to autoconf2.69

 -- Bastian Germann <bage@debian.org>  Mon, 26 Jun 2023 23:42:47 +0200

gcc-avr (1:5.4.0+Atmel3.6.2-5) unstable; urgency=medium

  * Basic file clean-up for source format switch
  * Convert to source format 3.0 (quilt)
  * d/copyright: Remove outdated URL

 -- Bastian Germann <bage@debian.org>  Mon, 26 Jun 2023 13:58:20 +0200

gcc-avr (1:5.4.0+Atmel3.6.2-4) unstable; urgency=medium

  * Remove versions from automake, tar, dpkg B-D (Closes: #865163)
  * Adopt package (Closes: #969203)

 -- Bastian Germann <bage@debian.org>  Thu, 22 Jun 2023 22:45:53 +0200

gcc-avr (1:5.4.0+Atmel3.6.2-3) unstable; urgency=medium

  * Disable libcc1 (closes: #1003030)
  * Bump debhelper compat to 7 (closes: #965539)

 -- Hakan Ardo <hakan@debian.org>  Fri, 07 Jan 2022 10:59:48 +0100

gcc-avr (1:5.4.0+Atmel3.6.2-2) unstable; urgency=medium

  * Applied patch from Rafael Laboissière <rafael@debian.org> fixing gcc-
    11 compile error (closes: #984148)

 -- Hakan Ardo <hakan@debian.org>  Sun, 21 Nov 2021 10:08:16 +0100

gcc-avr (1:5.4.0+Atmel3.6.2-1) unstable; urgency=medium

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Tue, 14 Jan 2020 20:31:47 +0100

gcc-avr (1:5.4.0+Atmel3.6.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces
  * d/control: Add Vcs-* field

  [ Hakan Ardo ]
  * Applied upstream patch
    https://gcc.gnu.org/viewcvs/gcc?view=revision&revision=261621
    removing illegal coercion of an unaligned pointer value (closes:
    #914913)

 -- Hakan Ardo <hakan@debian.org>  Thu, 27 Dec 2018 21:06:12 +0100

gcc-avr (1:5.4.0+Atmel3.6.1-1) unstable; urgency=medium

  * New upstream release
  * No longer suggests task-c-devel (closes: #898195)
  * No longer forces CC=gcc (closes: #899120)

 -- Hakan Ardo <hakan@debian.org>  Tue, 26 Jun 2018 08:46:09 +0200

gcc-avr (1:5.4.0+Atmel3.6.0-1) unstable; urgency=medium

  * New upstream release (closes: #798205)
  * Package now maintained in a git repository set up by
    Gioele Barabucci <gioele@svario.it>

 -- Hakan Ardo <hakan@debian.org>  Thu, 27 Jul 2017 14:03:57 +0200

gcc-avr (1:4.9.2+Atmel3.5.4-1) unstable; urgency=medium

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Tue, 04 Apr 2017 20:07:00 +0200

gcc-avr (1:4.9.2+Atmel3.5.3-1) unstable; urgency=medium

  * New upstream release
  * Applied patch upstream patch
    https://gcc.gnu.org/git/?p=gcc.git;a=commitdiff;h=ec1cc0263f156f706
    93a62cf17b254a0029f4852 to support compilation with gcc 6 (closes:
    #831174)

 -- Hakan Ardo <hakan@debian.org>  Sun, 28 Aug 2016 19:45:37 +0200

gcc-avr (1:4.9.2+Atmel3.5.0-1) unstable; urgency=medium

  * New upstream release (closes: #790103)

 -- Hakan Ardo <hakan@debian.org>  Sun, 06 Dec 2015 12:52:43 +0100

gcc-avr (1:4.8.1+Atmel3.4.5-1) unstable; urgency=medium

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Wed, 01 Apr 2015 20:12:15 +0200

gcc-avr (1:4.8.1+Atmel3.4.4-2) unstable; urgency=medium

  * Reduced sugestion on gcc-4.8 to gcc (>= 4:4.8) (closes: #751301)

 -- Hakan Ardo <hakan@debian.org>  Mon, 04 Aug 2014 12:29:37 +0200

gcc-avr (1:4.8.1+Atmel3.4.4-1) unstable; urgency=medium

  * New upstream release from Atmel-AVR-GNU-Toolchain v3.4.4
    (http://distribute.atmel.no/tools/opensource/Atmel-AVR-GNU-
    Toolchain/3.4.4/)
  * Removed dh_undocumented
  * Include full copyright statement
  * Added ${misc:Depends} dependency
  * Added build-arch and build-indep targets
  * Run dh_autoreconf during build (closes: #744576)

 -- Hakan Ardo <hakan@debian.org>  Sun, 01 Jun 2014 21:53:21 +0200

gcc-avr (1:4.8-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch to automake1.11. (Closes: #724374)

 -- Eric Dorland <eric@debian.org>  Sat, 15 Mar 2014 00:53:49 -0400

gcc-avr (1:4.8-2) unstable; urgency=low

  * Updated depenencies (closes: #725770)

 -- Hakan Ardo <hakan@debian.org>  Tue, 08 Oct 2013 19:59:30 +0200

gcc-avr (1:4.8-1) unstable; urgency=low

  * New upstream release (closes: #701280, #684226)

 -- Hakan Ardo <hakan@debian.org>  Sat, 05 Oct 2013 10:35:13 +0200

gcc-avr (1:4.7.2-2) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Add Built-Using for gcc-4.7-source. (closes: #696423)

 -- Hakan Ardo <hakan@debian.org>  Tue, 25 Dec 2012 20:27:57 +0100

gcc-avr (1:4.7.2-1) unstable; urgency=low

  * New upstream release (closes: #693572)

 -- Hakan Ardo <hakan@debian.org>  Tue, 27 Nov 2012 18:27:08 +0100

gcc-avr (1:4.7.0-2) unstable; urgency=low

  * New upstream release (closes: #674077)
  * Use build scripts from gcc 4.7 (closes: #673568)

 -- Hakan Ardo <hakan@debian.org>  Wed, 23 May 2012 09:36:40 +0200

gcc-avr (1:4.7.0-1) experimental; urgency=low

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Mon, 19 Mar 2012 07:47:46 +0100

gcc-avr (1:4.5.3-4) unstable; urgency=low

  * Backported patch for gcc bug 50289:
    http://gcc.gnu.org/bugzilla/show_bug.cgi?id=50289
  * Backported patch for gcc bug 46779:
    http://gcc.gnu.org/bugzilla/show_bug.cgi?id=46779

 -- Hakan Ardo <hakan@debian.org>  Wed, 08 Feb 2012 20:07:10 +0100

gcc-avr (1:4.5.3-3) unstable; urgency=low

  * Applied patch from peter green <plugwash@p10link.net> that removes
    the --disable-checking configure option (closes: #645822)

 -- Hakan Ardo <hakan@debian.org>  Sun, 20 Nov 2011 10:34:42 +0100

gcc-avr (1:4.5.3-2) unstable; urgency=low

  * Added 57-ctors-dtors-patch.diff patch that fixes GCC bug 45263. It
    was ported from gcc cvs by Francois Lorrain
    <francois.lorrain@gmail.com> and confirmed by Changwoo Ryu
    <cwryu@debian.org> (closes: #634341)

 -- Hakan Ardo <hakan@debian.org>  Thu, 11 Aug 2011 18:50:38 +0200

gcc-avr (1:4.5.3-1) unstable; urgency=low

  * New upstream release (closes: #550985, #594279, #629734)
  * Replaced the patch-set with the patch-set provided by Atmel at
    http://distribute.atmel.no/tools/opensource/avr-gcc/gcc-4.5.1/
         30-gcc-4.5.1-fixedpoint-3-4-2010.patch
         31-gcc-4.5.1-xmega-v14.patch
         32-gcc-4.5.1-avrtiny10.patch
         33-gcc-4.5.1-osmain.patch
         34-gcc-4.5.1-builtins-v6.patch
         35-gcc-4.5.1-avrtiny10-non-fixedpoint.patch
         37-gcc-4.5.1-option-list-devices.patch
         38-gcc-4.5.1-bug13473.patch
         39-gcc-4.5.1-bug13579.patch
         40-gcc-4.5.1-bug-18145-v4.patch
         41-gcc-4.5.1-avrtiny10-bug-12510.patch
         42-gcc-4.5.1-bug12915.patch
         43-gcc-4.5.1-bug13932.patch
         44-gcc-4.5.1-bug13789.patch
         50-gcc-4.5.1-new-devices.patch
         51-gcc-4.5.1-atmega32_5_50_90_pa.patch
         54-gcc-4.5.1-attiny1634.patch
         56-gcc-4.5.1-atmega48pa.patch

 -- Hakan Ardo <hakan@debian.org>  Sat, 09 Jul 2011 19:26:28 +0200

gcc-avr (1:4.3.5-1) unstable; urgency=low

  * New upstream release
  * Removed build-depedns on dbs (closes: #576067)
  * Updated the patch-set from
    http://www.freebsd.org/cgi/cvsweb.cgi/ports/devel/avr-gcc/files/:
         patch-avr-libgcc.S
         patch-newdevices
         patch-xmega
         patch-xx-os_main
         patch-bug19636-24894-31644-31786
         patch-bug33009
         patch-bug34210-35508
         patch-bug35013
         patch-bug11259
         patch-bug18145
         patch-builtins
         patch-disable-ssp
         patch-param-inline-call-cost



 -- Hakan Ardo <hakan@debian.org>  Fri, 30 Jul 2010 09:59:34 +0200

gcc-avr (1:4.3.4-1) unstable; urgency=low

  * New upstream release (closes: #543016, #491399)
  * Applied patches from
    http://www.freebsd.org/cgi/cvsweb.cgi/ports/devel/avr-gcc/files/:
        patch-avr-libgcc.S (rev 1.1)
        patch-newdevices (rev 1.20)
        patch-xmega (rev 1.2)
        patch-xx-os_main (rev 1.1)
        patch-bug19636-24894-31644-31786 (rev 1.1)
        patch-bug33009 (rev 1.1)
        patch-bug34210-35508 (rev 1.1)
        patch-bug35013 (rev 1.1)


 -- Hakan Ardo <hakan@debian.org>  Sat, 31 Oct 2009 11:01:45 +0100

gcc-avr (1:4.3.3-1) unstable; urgency=low

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Sun, 25 Jan 2009 16:10:20 +0100

gcc-avr (1:4.3.2-1) unstable; urgency=low

  * Removed patches 40-1-gcc-4.3.0-bug-30243, 41-0-gcc-4.3.0-bug-34932
    and 40-9-gcc-4.3.0-bug-35542 as they has been incoporated
    upstream in the code provided by gcc-4.3-source 4.3.2-1 (closes:
    #501424).

 -- Hakan Ardo <hakan@debian.org>  Sat, 11 Oct 2008 09:07:49 +0200

gcc-avr (1:4.3.0-4) unstable; urgency=low

  * No longer applies 40-8-gcc-4.3.0-bug-35519 and
    41-1-gcc-4.3.0-bug-leaf as the src provided
    by gcc-4.3-source 4.3.1-6 already has this patch applied (closes:
    #490310)

 -- Hakan Ardo <hakan@debian.org>  Mon, 21 Jul 2008 18:22:33 +0200

gcc-avr (1:4.3.0-3) unstable; urgency=low

  * Revered to an unpatched 4.3.0 release
  * Applied WinAVR-20080610 from patches/gcc/4.3.0/ in cvs server
    anonymous@winavr.cvs.sourceforge.net:/cvsroot/winavr with tag
    WinAVR-20080610:
      20-gcc-4.3.0-libiberty-Makefile.in.patch
      21-gcc-4.3.0-disable-libssp.patch
      40-0-gcc-4.3.0-bug-10768.patch
      40-1-gcc-4.3.0-bug-30243.patch
      40-2-gcc-4.3.0-bug-11259_v3.patch
      40-3-gcc-4.3.0-bug-spill3.patch
      40-4-gcc-4.3.0-bug-35013.patch
      40-6-gcc-4.3.0-libgcc16.patch
      40-7-gcc-4.3.0-bug-33009.patch
      40-8-gcc-4.3.0-bug-35519.patch
      40-9-gcc-4.3.0-bug-35542.patch
      41-0-gcc-4.3.0-bug-34932.patch
      41-1-gcc-4.3.0-bug-leaf.patch
      50-gcc-4.3.0-mega256.patch
      51-gcc-4.3.0-xmega-v9.patch
      52-gcc-4.3.0-atmega32m1.patch
      53-gcc-4.3.0-atmega32c1.patch
      54-gcc-4.3.0-atmega32u4.patch
      55-gcc-4.3.0-attiny167.patch
      56-gcc-4.3.0-remove-atmega32hvb.patch
      60-gcc-4.3.0-ada-xgnatugn.patch
      61-gcc-4.3.0-osmain.patch

 -- Hakan Ardo <hakan@debian.org>  Wed, 02 Jul 2008 11:01:26 +0200

gcc-avr (1:4.3.0-2) unstable; urgency=low

  * libiberty.a now removed for X86_64 arch aswell (closes: #474674)

 -- Hakan Ardo <hakan@debian.org>  Mon, 07 Apr 2008 16:36:22 +0200

gcc-avr (1:4.3.0-1) unstable; urgency=low

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Fri, 04 Apr 2008 11:37:41 +0200

gcc-avr (1:4.2.3-1) unstable; urgency=low

  * Moved /usr/libexec/gcc/ to /usr/lib/gcc/ (closes: #457213)
  * Fixed 0b patch to handle dsfg free docs (closes: #464260)
  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Thu, 07 Feb 2008 15:00:49 +0100

gcc-avr (1:4.2.2-1) unstable; urgency=low

  * New upstream release.

 -- Hakan Ardo <hakan@debian.org>  Sat, 15 Dec 2007 16:39:42 +0100

gcc-avr (1:4.2.1-2) unstable; urgency=low

  * Recompiled with new binutils

 -- Hakan Ardo <hakan@debian.org>  Tue, 14 Aug 2007 18:12:04 +0200

gcc-avr (1:4.2.1-1) unstable; urgency=low

  * New upstream release (includes newdevices patch from freeBSD) (closes: #420061)
  * Now uses unpack rules from gcc-source package (closes: #421142)
  * Applied binary constants patch (closes: #426940)

 -- Hakan Ardo <hakan@debian.org>  Thu,  9 Aug 2007 09:04:11 +0200

gcc-avr (1:4.1.2-1) unstable; urgency=low

  * New upstream release

 -- Hakan Ardo <hakan@debian.org>  Mon, 30 Apr 2007 10:46:13 +0200

gcc-avr (1:4.1.1-1) unstable; urgency=low

  * New upstream release (closes: #415917)
  * Now uses gcc-4.1-source (closes: #413214)

 -- Hakan Ardo <hakan@debian.org>  Sun, 22 Apr 2007 17:17:59 +0200

gcc-avr (1:4.1.0.dfsg.1-1) unstable; urgency=low

  [ Frank Küster ]
  * Add a makefile snippet to remove files from the tarballs that are
    under GFDL and contain invariant sections.  Thanks to Matthias Klose
    for the list of files
  * Let the clean target depend on the repack-gfdl target, and do the
    cleaning (closes: #413216)

  [ Hakan Ardo ]
  * Applied patch from Frank above
  * Removed info files now recreated as empty files to allow compilation
    as sugested by Matthias Klose
  * Removed clean's dependency on repack-gfdl

 -- Hakan Ardo <hakan@debian.org>  Mon, 19 Mar 2007 18:48:37 +0100

gcc-avr (1:4.1.0-1) unstable; urgency=low

  * Upstream release
  * Removed debian/rules.old
  * Now suggests gcc-4.1 instead (closes: #353267)

 -- Hakan Ardo <hakan@debian.org>  Mon,  1 May 2006 11:27:41 +0200

gcc-avr (1:4.0.2-1) unstable; urgency=low

  * New upstream release
  * Added missing avr-gccbug man page (closes: #321221)
  * Removed duplicated Suggests fields from control

 -- Hakan Ardo <hakan@debian.org>  Sat,  4 Feb 2006 17:39:37 +0100

gcc-avr (1:3.4.3-2) unstable; urgency=low

  * Rebuilt with new binutils adding support for atmega48, atmega88,
    atmega168, attiny13, attiny2313 and at90can128.

 -- Hakan Ardo <hakan@debian.org>  Sun, 27 Feb 2005 18:26:55 +0100

gcc-avr (1:3.4.3-1) unstable; urgency=low

  * New upstream release
  * Applied patch from Theodore A. Roth enabling new avr devices,
    including at90can128
  * No longer released as native package (closes: #270421)
  * Added info to README.debian on how to compile nesC/TinyOS
  * Removed --with-as=/usr/avr/bin/as from configure, which overrided -B
    (closes: #275995)

 -- Hakan Ardo <hakan@debian.org>  Sun, 26 Dec 2004 15:48:13 +0100

gcc-avr (1:3.4.1-1) unstable; urgency=low

  * Upstream update
  * All cross libs now striped with avr-strip (closes: #264686)

 -- Hakan Ardo <hakan@debian.org>  Wed, 11 Aug 2004 20:33:26 +0200

gcc-avr (1:3.4.0-1) unstable; urgency=low

  * Upstream release (closes: #241835)

 -- Hakan Ardo <hakan@debian.org>  Sat, 15 May 2004 17:19:35 +0200

gcc-avr (1:3.3.2-1) unstable; urgency=low

  * Upstream update (closes: #160051)
  * Readded Build-Depends on toolchain-source (closes: #207189, #207493)
  * Updated copyright

 -- Hakan Ardo <hakan@debian.org>  Mon, 29 Mar 2004 14:58:18 +0200

gcc-avr (1:3.2.90.20030512) unstable; urgency=low

  * Updated to 030512 snapshot
  * Now contains src tar balls
  * Now uses packhed dbs
  * Updated build-depend on binutils-avr

 -- Hakan Ardo <hakan@debian.org>  Wed, 25 Jun 2003 21:04:42 +0200

gcc-avr (1:3.2.3.cvs20030221-3) unstable; urgency=low

  * Now builddepends on automake1.4 (closes: #184760)

 -- Hakan Ardo <hakan@debian.org>  Sun, 16 Mar 2003 11:51:41 +0100

gcc-avr (1:3.2.3.cvs20030221-2) unstable; urgency=low

  * libgcc.a was messed up by dh_strip (closes: #183062)
  * Recompiled with toolchain-source 3.2-7

 -- Hakan Ardo <hakan@debian.org>  Tue, 11 Mar 2003 16:07:30 +0100

gcc-avr (1:3.2.3.cvs20030221-1) unstable; urgency=low

  * New upstream release (3.2.3.cvs20030221)

 -- Hakan Ardo <hakan@debian.org>  Wed, 26 Feb 2003 12:24:51 +0100

gcc-avr (1:3.2-3) unstable; urgency=low

  * Recompiled with toolchain-source 3.2-3

 -- Hakan Ardo <hakan@debian.org>  Thu,  7 Nov 2002 22:53:11 +0100

gcc-avr (1:3.2-2) unstable; urgency=low

  * Relaxed dependency on native pkg and added README.debian describing
    the case (closes: #161060)
  * Recompiled with toolchain-source 3.2-2

 -- Hakan Ardo <hakan@debian.org>  Fri, 18 Oct 2002 09:41:32 +0200

gcc-avr (1:3.2-1) unstable; urgency=low

  * New upstream release (3.2)

 -- Hakan Ardo <hakan@debian.org>  Sun, 18 Aug 2002 16:38:47 +0200

gcc-avr (1:3.1.1-1) unstable; urgency=low

  * New upstream release (3.1.1)

 -- Hakan Ardo <hakan@debian.org>  Fri,  2 Aug 2002 16:15:39 +0200

gcc-avr (1:3.1-3) unstable; urgency=low

  * Recompiled with toolchain-source 3.1-3 (closes: #142646)

 -- Hakan Ardo <hakan@debian.org>  Sat,  6 Jul 2002 17:13:27 +0200

gcc-avr (1:3.1-2) unstable; urgency=low

  * Recompiled with toolchain-source 3.1-2 (closes: #148778)

 -- Hakan Ardo <hakan@debian.org>  Fri,  5 Jul 2002 09:51:20 +0200

gcc-avr (1:3.1-1) unstable; urgency=low

  * New upstream release (3.1)

 -- Hakan Ardo <hakan@debian.org>  Fri, 31 May 2002 13:57:13 +0200

gcc-avr (1:3.0.4-4) unstable; urgency=low

  * Recompiled with toolchain-source 3.0.4-5 (closes: #143172)

 -- Hakan Ardo <hakan@debian.org>  Thu,  2 May 2002 15:48:16 +0200

gcc-avr (1:3.0.4-3) unstable; urgency=low

  * Recompiled with toolchain-source 3.0.4-4

 -- Hakan Ardo <hakan@debian.org>  Mon,  8 Apr 2002 11:57:54 +0200

gcc-avr (1:3.0.4-2) unstable; urgency=low

  * Recompiled with toolchain-source 3.0.4-3

 -- Hakan Ardo <hakan@debian.org>  Sat, 30 Mar 2002 12:42:19 +0100

gcc-avr (1:3.0.4-1) unstable; urgency=low

  * New upstream release (3.0.4)
  * Fixed man file removal (closes: #137586)

 -- Hakan Ardo <hakan@debian.org>  Mon, 11 Mar 2002 15:18:57 +0100

gcc-avr (1:3.0.3-3) unstable; urgency=low

  * Removed som duplicated man pages
  * Renamed avr-linux to avr as we dont run linux on target
  * Recompiled with toolchain-source 3.0.3-3

 -- Hakan Ardo <hakan@debian.org>  Sat,  9 Feb 2002 15:50:16 +0100

gcc-avr (1:3.0.3-2) unstable; urgency=low

  * Now uses tar tjf (Bug #128550)
  * Removed som unecessery build constraints
  * Recompiled with toolchain-source 3.0.3-2

 -- Hakan Ardo <hakan@debian.org>  Thu, 10 Jan 2002 23:44:11 +0100

gcc-avr (1:3.0.3-1) unstable; urgency=low

  * gcc version no longer hardcoded in rules
  * Upstream update to 3.0.3
  * Updated Build-Depends on binutils

 -- Hakan Ardo <hakan@debian.org>  Tue,  8 Jan 2002 17:42:27 +0100

gcc-avr (3.0.ds9-7) unstable; urgency=low

  * Removed dependency on graphviz and updated version number of
    toolchain-source (Bug #108902)

 -- Hakan Ardo <hakan@debian.org>  Sun, 19 Aug 2001 22:12:10 +0200

gcc-avr (3.0.ds9-6) unstable; urgency=low

  * Dummy release to get pkg back in distribution (Bug #106872)

 -- Hakan Ardo <hakan@debian.org>  Wed,  8 Aug 2001 18:21:53 +0200

gcc-avr (3.0.ds9-5) unstable; urgency=low

  * .deb file no longer in src pkg
  * Now build-depends on toolchain-source instaed of using apt-get
    (Bug #105847)

 -- Hakan Ardo <hakan@debian.org>  Thu, 26 Jul 2001 13:09:26 +0200

gcc-avr (3.0.ds9-4) unstable; urgency=low

  * Initial Release

 -- Hakan Ardo <hakan@debian.org>  Mon,  2 Jul 2001 22:45:33 +0200

Local variables:
mode: debian-changelog
End:
